package TaskForInternship;

import java.util.Arrays;
import java.util.regex.Pattern;

public class DefiningAction {

    IAction theAction = new Action();

    public void defineAction(String message, DatabaseOfArrays databaseOfArrays) {

        if (Pattern.matches("init [0-9\\s]+", message)) {
            int[] arrayOfNumbers = Arrays.stream(message.substring(5).split("\\s")).mapToInt(Integer::parseInt)
                    .sorted().toArray();
            theAction.makeArrays(arrayOfNumbers, databaseOfArrays);
        }

        //check if all the entered numbers are in arrays
        if (message.equals("anyMore")) {
            theAction.checkOdds(databaseOfArrays);
        }

        //display arrays
        if (message.equals("print")) {
            theAction.printAll(databaseOfArrays);
        }

        if (message.equals("print X")) {
            theAction.printXthArray(databaseOfArrays);
        }
        if (message.equals("print S")) {
            theAction.printSthArray(databaseOfArrays);
        }
        if (message.equals("print M")) {
            theAction.printMthArray(databaseOfArrays);
        }

        //clear arrays
        if (message.equals("clear X")) {
            theAction.clearXthArray(databaseOfArrays);
        }
        if (message.equals("clear S")) {
            theAction.clearSthArray(databaseOfArrays);
        }
        if (message.equals("clear M")) {
            theAction.clearMthArray(databaseOfArrays);
        }

        //merge all arrays into one, display this array and clear rest arrays
        if (message.equals("merge")) {
            theAction.mergeArrays(databaseOfArrays);
        }

        if (message.equals("help")) {
            theAction.getInfo();
        }
    }
}
