package TaskForInternship;

public interface IAction {

    //init arrays
    public void makeArrays(int[] arrayOfNumbers, DatabaseOfArrays databaseOfArrays);

    //check if all entered numbers are in arrays
    public void checkOdds(DatabaseOfArrays databaseOfArrays);

    //display all/Xth/Sth/Mth array
    public void printAll(DatabaseOfArrays databaseOfArrays);
    public void printXthArray(DatabaseOfArrays databaseOfArrays);
    public void printSthArray(DatabaseOfArrays databaseOfArrays);
    public void printMthArray(DatabaseOfArrays databaseOfArrays);

    //clear Xth, Sth, Mth array
    public void clearXthArray(DatabaseOfArrays databaseOfArrays);
    public void clearSthArray(DatabaseOfArrays databaseOfArrays);
    public void clearMthArray(DatabaseOfArrays databaseOfArrays);

    //display the array composed of Xth, Sth, Mth arrays and clear Xth, Sth, Mth arrays
    public void mergeArrays(DatabaseOfArrays databaseOfArrays);

    //display info on how to use the programm
    public void getInfo();

}
